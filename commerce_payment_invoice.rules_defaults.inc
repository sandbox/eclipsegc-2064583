<?php
/**
 * @file
 * commerce_payment_invoice.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_payment_invoice_default_rules_configuration() {
  $items = array();
  $items['rules_closing_an_invoiced_order'] = entity_import('rules_config', '{ "rules_closing_an_invoiced_order" : {
      "LABEL" : "Closing an Invoiced Order",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "commerce_payment", "rules", "entity" ],
      "ON" : [ "commerce_order_presave" ],
      "IF" : [
        { "commerce_payment_selected_payment_method" : {
            "commerce_order" : [ "commerce_order" ],
            "method_id" : "commerce_payment_invoice"
          }
        },
        { "NOT data_is" : {
            "data" : [ "commerce-order-unchanged:status" ],
            "value" : [ "commerce-order:status" ]
          }
        },
        { "data_is" : { "data" : [ "commerce-order:status" ], "value" : "invoice" } }
      ],
      "DO" : [
        { "entity_query" : {
            "USING" : {
              "type" : "commerce_payment_transaction",
              "property" : "order_id",
              "value" : [ "commerce-order:order-id" ],
              "limit" : "1"
            },
            "PROVIDE" : { "entity_fetched" : { "transaction" : "Transaction" } }
          }
        },
        { "component_rules_invoice_transaction_update" : { "transaction" : [ "transaction:0" ] } }
      ]
    }
  }');
  $items['rules_invoice_transaction_update'] = entity_import('rules_config', '{ "rules_invoice_transaction_update" : {
      "LABEL" : "Invoice Transaction Update",
      "PLUGIN" : "rule",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "transaction" : { "label" : "Transaction", "type" : "commerce_payment_transaction" } },
      "IF" : [
        { "data_is" : {
            "data" : [ "transaction:payment-method" ],
            "value" : "commerce_payment_invoice"
          }
        },
        { "data_is" : { "data" : [ "transaction:status" ], "value" : "pending" } }
      ],
      "DO" : [
        { "data_set" : { "data" : [ "transaction:status" ], "value" : "success" } },
        { "entity_save" : { "data" : [ "transaction" ], "immediate" : 1 } }
      ]
    }
  }');
  return $items;
}
